package com.netradius.iam.directauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectAuthApplication {

  public static void main(String[] args) {
    SpringApplication.run(DirectAuthApplication.class, args);
  }

}
