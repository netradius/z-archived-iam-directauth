package com.netradius.iam.directauth.handler;

import com.netradius.commons.lang.StringHelper;
import lombok.Getter;

import java.util.UUID;
import java.util.regex.Pattern;

public class Resource {

  public static final String PATTERN_STR = "^[a-zA-Z0-9]+$";
  public static final Pattern PATTERN = Pattern.compile(PATTERN_STR);

  private Resource() {}

  @Getter
  private String type;

  @Getter
  private UUID id;

  @Override
  public String toString() {
    return type + "/" + id.toString();
  }

  private static void validateType(String type) {
    if (StringHelper.isBlank(type)) {
      throw new IllegalArgumentException("Type in resource may not be blank");
    }
    if (!PATTERN.matcher(type).matches()) {
      throw new IllegalArgumentException(
          String.format("Invalid type %s. Type must match pattern %s", type, PATTERN_STR));
    }
  }

  private static UUID parseId(String id) {
    if (StringHelper.isBlank(id)) {
      throw new IllegalArgumentException("ID in resource may not be blank");
    }
    try {
      return UUID.fromString(id);
    } catch (IllegalArgumentException x) {
      throw new IllegalArgumentException(
          String.format("Invalid resource ID %s. A UUID is required.", id));
    }
  }

  public static Resource parse(String resource) {
    int idx = resource.indexOf('/');
    if (idx == -1) {
      throw new IllegalArgumentException(
          String.format("Invalid resource %s. Resource must be formatted type/id", resource));
    }
    String type = resource.substring(0, idx);
    validateType(type);
    UUID id = parseId(resource.substring(idx));
    Resource r = new Resource();
    r.type = type.toLowerCase();
    r.id = id;
    return r;
  }
}
