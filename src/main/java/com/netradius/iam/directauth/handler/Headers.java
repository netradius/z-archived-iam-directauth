package com.netradius.iam.directauth.handler;

import com.netradius.authlib.direct.DirectAuthorization;
import com.netradius.commons.lang.StringHelper;
import lombok.Getter;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.List;
import java.util.UUID;

public class Headers {

  public static final String AUTHORIZATION_HEADER = "authorization";
  public static final String CLIENT_HEADER = "authorization-client";
  public static final String AUDIENCE_HEADER = "authorization-audience";
  public static final String PATH_HEADER = "authorization-path";
  public static final String ACTION_HEADER = "authorization-action";
  public static final String RESOURCE_HEADER = "authorization-resource";

  private Headers() {}

  @Getter
  private DirectAuthorization authorization;

  @Getter
  private DirectAuthorization clientAuthorization;

  @Getter
  private Audience audience;

  @Getter
  private String path;

  @Getter
  private String action;

  @Getter
  private Resource resource;

  private static String getHeader(ServerRequest request, String header) {
    List<String> values = request.headers().header(header);
    return values.isEmpty() ? null : values.get(0);
  }

  private static String getRequiredHeader(ServerRequest request, String header) {
    String value = getHeader(request, header);
    if (value == null) {
      throw new IllegalArgumentException(String.format("HTTP header %s is required", header));
    }
    return value;
  }

  private static DirectAuthorization getAuthorization(String clientAuthorization, String audience, String path) {
    return DirectAuthorization.fromHeaderValue(clientAuthorization, audience, path);
  }

  private static UUID getResource(ServerRequest request) {
    String value = getHeader(request, RESOURCE_HEADER);
    if (StringHelper.isBlank(value)) {
      return null;
    }
    try {
      return UUID.fromString(value);
    } catch (IllegalArgumentException x) {
      throw new IllegalArgumentException(String.format("Invalid resource %s. A UUID is required.", value));
    }
  }

  public static Headers parse(Audience iamAudience, String iamPath, ServerRequest request) {
    String authorization = getRequiredHeader(request, AUTHORIZATION_HEADER);
    DirectAuthorization directAuthorization = getAuthorization(authorization, iamAudience.toString(), iamPath);

    String audienceStr = getRequiredHeader(request, AUDIENCE_HEADER);
    Audience audience = Audience.parse(audienceStr);
    String path = getRequiredHeader(request, PATH_HEADER);
    String clientAuthorization = getRequiredHeader(request, CLIENT_HEADER);
    DirectAuthorization clientDirectAuthorization = getAuthorization(clientAuthorization, audienceStr, path);

    String action = getRequiredHeader(request, ACTION_HEADER);

    String resourceStr = getHeader(request, RESOURCE_HEADER);
    Resource resource = null;
    if (StringHelper.isNotBlank(resourceStr)) {
      resource = Resource.parse(resourceStr);
    }

    Headers headers = new Headers();
    headers.authorization = directAuthorization;
    headers.clientAuthorization = clientDirectAuthorization;
    headers.audience = audience;
    headers.path = path;
    headers.action = action;
    headers.resource = resource;
    return headers;
  }
}
