package com.netradius.iam.directauth.handler;

import com.netradius.commons.lang.StringHelper;
import lombok.Data;
import lombok.Getter;

import java.util.regex.Pattern;

@Data
public class Audience {

  public static final String PATTERN_STR = "^[a-zA-Z0-9]+$";
  public static final Pattern PATTERN = Pattern.compile(PATTERN_STR);

  private Audience() {}

  @Getter
  private String namespace;

  @Getter
  private String service;

  @Override
  public String toString() {
    return namespace + "/" + service;
  }

  private static void validateNamespace(String namespace) {
    if (StringHelper.isBlank(namespace)) {
      throw new IllegalArgumentException("Namespace in audience may not be blank");
    }
    if (!PATTERN.matcher(namespace).matches()) {
      throw new IllegalArgumentException(
          String.format("Invalid namespace %s. Namespace must match pattern %s", namespace, PATTERN_STR));
    }
  }

  private static void validateService(String service) {
    if (StringHelper.isBlank(service)) {
      throw new IllegalArgumentException("Service in audience may not be blank");
    }
    if (!PATTERN.matcher(service).matches()) {
      throw new IllegalArgumentException(
          String.format("Invalid service %s. Service must match pattern %s", service, PATTERN_STR));
    }
  }

  public static Audience parse(String audience) {
    int idx = audience.indexOf('/');
    if (idx == -1) {
      throw new IllegalArgumentException(
          String.format("Invalid audience %s. Audience must be formatted namespace/service.", audience));
    }
    String namespace = audience.substring(0, idx);
    validateNamespace(namespace);
    String service = idx < audience.length() ? audience.substring(idx + 1) : "";
    validateService(service);
    Audience a = new Audience();
    a.namespace = namespace.toLowerCase();
    a.service = service.toLowerCase();
    return a;
  }
}
