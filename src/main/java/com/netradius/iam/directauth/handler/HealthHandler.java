package com.netradius.iam.directauth.handler;

import com.netradius.iam.directauth.data.AuthDataRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class HealthHandler {

  public static final String SHALLOW_PATH = "/authorize/direct/health";
  public static final String DEEP_PATH = "/authorize/direct/health/deep";

  private AuthDataRepository repository;

  public HealthHandler(AuthDataRepository repository) {
    this.repository = repository;
  }

  public Mono<ServerResponse> shallow(ServerRequest request) {
    return ServerResponse.ok()
        .body(BodyInserters.empty());
  }

  public Mono<ServerResponse> deep(ServerRequest request) {
    Mono<Integer> mono = repository.health();
    return mono.flatMap(i -> ServerResponse.ok().body(BodyInserters.empty()));
  }
}
