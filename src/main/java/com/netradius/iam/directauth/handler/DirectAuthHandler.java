package com.netradius.iam.directauth.handler;

import com.netradius.authlib.direct.DirectAuthSecret;
import com.netradius.authlib.direct.DirectAuthType;
import com.netradius.authlib.direct.DirectAuthorization;
import com.netradius.authlib.exception.AuthFailedException;
import com.netradius.iam.directauth.data.AuthData;
import com.netradius.iam.directauth.data.AuthDataRepository;
import com.netradius.iam.directauth.error.InvalidHeaderException;
import com.netradius.iam.directauth.error.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Slf4j
@Component
public class DirectAuthHandler {

  public static final String PATH = "/authorize/direct";
  public static final String ACTION = "directauth";
  public static final String ACCOUNT_HEADER = "account";
  public static final String APPLICATION_HEADER = "application";

  private AuthDataRepository repository;
  private Audience audience;
  private long tolerance;

  public DirectAuthHandler(
      AuthDataRepository repository,
      @Value("${iam.audience}") String audience,
      @Value("${iam.tolerance}") long tolerance) {
    this.repository = repository;
    this.audience = Audience.parse(audience);
    this.tolerance = tolerance;
  }

  private Headers getHeaders(ServerRequest request) {
    try {
      return Headers.parse(this.audience, PATH, request);
    } catch (IllegalArgumentException x) {
      throw new InvalidHeaderException(x.getMessage());
    }
  }

  private Mono<AuthData> findAuthData(Headers headers) {
    String namespace = this.audience.getNamespace();
    String service = this.audience.getService();
    UUID keyId = headers.getAuthorization().getDecodedKeyId();
    DirectAuthType type = headers.getAuthorization().getType();
    return DirectAuthType.V1_HMAC_SHA512 == type
        ? repository.auth512(namespace, service, ACTION, keyId)
        : repository.auth1(namespace, service, ACTION, keyId);
  }

  private Mono<AuthData> findClientAuthData(Headers headers) {
    String namespace = headers.getAudience().getNamespace();
    String service = headers.getAudience().getService();
    String action = headers.getAction().toLowerCase();
    UUID keyId = headers.getClientAuthorization().getDecodedKeyId();
    Resource resource = headers.getResource();
    DirectAuthType type = headers.getAuthorization().getType();
    if (resource == null) {
      return DirectAuthType.V1_HMAC_SHA512 == type
          ? repository.auth512(namespace, service, action, keyId)
          : repository.auth1(namespace, service, action, keyId);
    } else {
      return DirectAuthType.V1_HMAC_SHA512 == type
          ? repository.auth512(namespace, service, action, keyId, resource.getType(), resource.getId())
          : repository.auth1(namespace, service, action, keyId, resource.getType(), resource.getId());
    }
  }

  private DirectAuthSecret toSecret(DirectAuthorization authorization, AuthData authData) {
    return DirectAuthSecret.builder().type(authorization.getType())
        .keyId(authData.getId()).secretKey(authData.getSecret()).build();
  }

  private AuthData authorize(DirectAuthorization authorization, AuthData authData) {
    if (authData.getAllowed() == null || !authData.getAllowed()) {
      throw new UnauthorizedException();
    }
    DirectAuthSecret secret = toSecret(authorization, authData);
    try {
      authorization.authorize(secret, tolerance, tolerance);
    } catch (AuthFailedException x) {
      throw new UnauthorizedException();
    }
    return authData;
  }

  private void logAuthData(AuthData authData) {
    if (authData != null) {
      if (log.isDebugEnabled()) {
        log.debug("Found auth data: " + authData.toString());
      }
    } else {
      log.debug("No auth data found");
    }
  }

  public Mono<ServerResponse> handle(ServerRequest request) {
    // Parse Request Headers
    // authorization -- resource servers authorization
    // client-authorization -- authorization header from client application
    // authorization-audience - nr/hello -- service receiving the request on the resource server
    // authorization-path - /hello -- path receiving the request on the resource server
    // authorization-action - Hello -- action being performed against the service (optional)
    // authorization-resource - ex. keys/a0b95912-da3f-4971-8e79-4b5c23f385da (optional)
    Headers headers = getHeaders(request);

    Mono<AuthData> authDataMono = findAuthData(headers)
        .doOnSuccess(this::logAuthData)
        .switchIfEmpty(Mono.error(new UnauthorizedException()))
        .map(authData -> authorize(headers.getAuthorization(), authData));

    Mono<AuthData> clientAuthDataMono = findClientAuthData(headers)
        .doOnSuccess(this::logAuthData)
        .switchIfEmpty(Mono.error(new UnauthorizedException()))
        .map(authData -> authorize(headers.getClientAuthorization(), authData));

    Mono<AuthData> mono = authDataMono.flatMap(authData -> clientAuthDataMono);

    // Send Response Headers
    // account -- UUID of the account
    // application -- UUID of the application
    return mono.flatMap(authData -> ServerResponse.ok()
          .header(ACCOUNT_HEADER, authData.getAccount().toString())
          .header(APPLICATION_HEADER, authData.getApplication().toString())
          .body(BodyInserters.empty()));
  }
}
