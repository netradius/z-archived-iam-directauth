package com.netradius.iam.directauth;

import com.netradius.iam.directauth.handler.DirectAuthHandler;
import com.netradius.iam.directauth.handler.HealthHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;

@Configuration
public class RouterConfig {

  @Bean
  public RouterFunction<ServerResponse> routeDirectAuth(DirectAuthHandler handler) {
    return RouterFunctions.route(POST(DirectAuthHandler.PATH), handler::handle);
  }

  @Bean
  public RouterFunction<ServerResponse> routeHealth(HealthHandler handler) {
    return RouterFunctions.route(GET(HealthHandler.SHALLOW_PATH), handler::shallow);
  }

  @Bean
  public RouterFunction<ServerResponse> routeDeep(HealthHandler handler) {
    return RouterFunctions.route(GET(HealthHandler.DEEP_PATH), handler::deep);
  }

  // authorization: <type> <keyid> <nonce> <timestamp> <signature> <derived-key-timestamp> <derived-key-expiration>

  // HelloWorld Server
  // Secret S0pdHNsMnxCzlQJkVTUODFqSC+JPvJuMImAPz4E1rZPZt5aLDe3rZIEs2WZk3CY+111WBX+Ej990BpMi1asuvg==
  //Key ID DziNKNNBQAiKG009VXf3Yg
  //Authorization V1-HMAC-SHA512 DziNKNNBQAiKG009VXf3Yg 1482581275 2020-05-15T03:53:40.212Z T30ugD6ESoN50NSd26WI9sha/UnN3jiIf5DeUgLx05Ndox3MLQH9P3zr4uoTD8ahxStFNvsmXGZ/FTznTQS5hw==

  // HelloWorld Client
  //Secret frUkGTiG6xm/7dGb5wmStuMIbZiYgPvVdYZTu2E8SkCAdq91ou2RMItA+L/iL2XilVttd7yzC9yQZSWzM4Ancg==
  //Derived Secret com.netradius.authlib.direct.DerivedSecret@5bf0d49
  //Key ID LYcpD_eYSWG4Y0FONxsTCA
  //Authorization V1-HMAC-SHA512 LYcpD_eYSWG4Y0FONxsTCA -53921179 2020-05-15T03:53:40.241Z GFwPZI5n6aZIVUyTb0aE3j1HZa2wIb6vV0RPQkmoO39mAMVm9bCcd97iweD8DcTXO31pUCK1nC1EUva4jG9BfQ== 2020-05-15T03:53:40.240Z 2020-05-15T04:53:40.240Z

  // Secret CUij71UK7z6wUpHWX9W9QWXgzLpkVRu6d8ndlNb+XMNqWX52GFBeFJdMXMK4sIaqS2y9ZsJzkkFk335nMdorKA==
  //Derived Secret com.netradius.authlib.direct.DerivedSecret@5bf0d49
  //Key ID tfavEAH0QHO5klI-CZXF-A
  //Authorization V1-HMAC-SHA512 tfavEAH0QHO5klI-CZXF-A -1250118566 2020-05-15T04:07:55.900Z bG42JyNlPAfZi11vkB5RraZ8nL7KBDsypttuBEVCb56V+ijKtTT41yritVe1DzYLkyN2hLRtyD+6NzKnPmu0EQ== 2020-05-15T04:07:55.898Z 2020-05-15T05:07:55.898Z

}
