package com.netradius.iam.directauth.error;

import org.springframework.http.HttpStatus;

public class InvalidHeaderException extends DomainException {

  public InvalidHeaderException(String message) {
    super(HttpStatus.BAD_REQUEST, message);
  }

}
