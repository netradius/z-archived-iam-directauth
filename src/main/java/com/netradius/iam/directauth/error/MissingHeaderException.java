package com.netradius.iam.directauth.error;

import org.springframework.http.HttpStatus;

public class MissingHeaderException extends DomainException {

  public MissingHeaderException(String header) {
    super(HttpStatus.BAD_REQUEST, String.format("Missing required header %s", header));
  }
}
