package com.netradius.iam.directauth.error;

import org.springframework.http.HttpStatus;

public class DomainException extends RuntimeException {

  private HttpStatus httpStatus;

  public DomainException(HttpStatus httpStatus, String message, Throwable error) {
    super(message, error);
    this.httpStatus = httpStatus;
  }

  public DomainException(HttpStatus httpStatus, String message) {
    this(httpStatus, message, null);
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

}
