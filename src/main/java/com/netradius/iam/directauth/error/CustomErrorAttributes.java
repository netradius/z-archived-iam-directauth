package com.netradius.iam.directauth.error;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;

@Component
public class CustomErrorAttributes<T extends Throwable> extends DefaultErrorAttributes {

  @Override
  public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
    Map<String, Object> errorAttributes = super.getErrorAttributes(request, options);
    Throwable error = getError(request);
    if (error instanceof DomainException) {
      DomainException de = (DomainException)error;
      errorAttributes.replace("status", de.getHttpStatus().value());
      errorAttributes.replace("error", de.getHttpStatus().getReasonPhrase());
    }
    return errorAttributes;
  }
}
