package com.netradius.iam.directauth.data;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface AuthDataRepository extends ReactiveCrudRepository<AuthData, UUID> {

  @Query("select a.account, k.id as id, k.application as application, k.secret as secret, bool_and(allow) as allowed " +
      "from application_v1_hmac_sha512_key k " +
      "    inner join application a on k.application = a.id " +
      "    inner join namespace n on a.account = n.account " +
      "    inner join policy_assignment pa on a.id = pa.application " +
      "    inner join policy_rule pr on pa.policy = pr.policy " +
      "    inner join service s on pr.service = s.id " +
      "    left outer join service_action sa on pr.action = sa.id " +
      "where lower(n.name) = :namespace " +
      "  and lower(s.name) = :service " +
      "  and (lower(sa.name) = :action or lower(sa.name) is null) " +
      "  and k.id = :keyId " +
      "  group by a.account, k.id, k.application, k.id, k.secret ")
  Mono<AuthData> auth512(String namespace, String service, String action, UUID keyId);

  @Query("select a.account, k.id as id, k.application as application, k.secret as secret, bool_and(allow) as allowed " +
      "from application_v1_hmac_sha1_key k " +
      "    inner join application a on k.application = a.id " +
      "    inner join namespace n on a.account = n.account " +
      "    inner join policy_assignment pa on a.id = pa.application " +
      "    inner join policy_rule pr on pa.policy = pr.policy " +
      "    inner join service s on pr.service = s.id " +
      "    left outer join service_action sa on pr.action = sa.id " +
      "where lower(n.name) = :namespace " +
      "  and lower(s.name) = :service " +
      "  and (lower(sa.name) = :action or lower(sa.name) is null) " +
      "  and k.id = :keyId " +
      "  group by a.account, k.id, k.application, k.id, k.secret ")
  Mono<AuthData> auth1(String namespace, String service, String action, UUID keyId);

  @Query("select a.account, k.id as id, k.application as application, k.secret as secret, bool_and(allow) as allowed " +
      "from application_v1_hmac_sha512_key k " +
      "         inner join application a on k.application = a.id " +
      "         inner join namespace n on a.account = n.account " +
      "         inner join policy_assignment pa on a.id = pa.application " +
      "         inner join policy_rule pr on pa.policy = pr.policy " +
      "         inner join service s on pr.service = s.id " +
      "         inner join service_resource_type srt on pr.resource_type = srt.id " +
      "         left outer join service_action sa on pr.action = sa.id " +
      "where lower(n.name) = :namespace " +
      "  and lower(s.name) = :service " +
      "  and (lower(sa.name) = :action or lower(sa.name) is null) " +
      "  and k.id = :keyId -- HelloServer " +
      "  and (lower(srt.name) = lower(:type))) " +
      "  and (pr.resource_id = :resourceId or pr.resource_id is null) " +
      "group by a.account, k.id, k.application, k.id, k.secret ")
  Mono<AuthData> auth512(String namespace, String service, String action, UUID keyId, String type, UUID resourceId);

  @Query("select a.account, k.id as id, k.application as application, k.secret as secret, bool_and(allow) as allowed " +
      "from application_v1_hmac_sha1_key k " +
      "         inner join application a on k.application = a.id " +
      "         inner join namespace n on a.account = n.account " +
      "         inner join policy_assignment pa on a.id = pa.application " +
      "         inner join policy_rule pr on pa.policy = pr.policy " +
      "         inner join service s on pr.service = s.id " +
      "         inner join service_resource_type srt on pr.resource_type = srt.id " +
      "         left outer join service_action sa on pr.action = sa.id " +
      "where lower(n.name) = :namespace " +
      "  and lower(s.name) = :service " +
      "  and (lower(sa.name) = :action or lower(sa.name) is null) " +
      "  and k.id = :keyId -- HelloServer " +
      "  and (lower(srt.name) = lower(:type)) " +
      "  and (pr.resource_id = :resourceId or pr.resource_id is null) " +
      "group by a.account, k.id, k.application, k.id, k.secret ")
  Mono<AuthData> auth1(String namespace, String service, String action, UUID keyId, String type, UUID resourceId);

  @Query("select 1")
  Mono<Integer> health();
}
