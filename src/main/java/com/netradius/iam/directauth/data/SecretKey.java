//package com.netradius.iam.directauth.data;
//
//import com.netradius.commons.bitsnbytes.BitTwiddler;
//import lombok.Data;
//import org.springframework.data.annotation.Id;
//
//import java.util.UUID;
//
//@Data
//public class SecretKey {
//
//  @Id
//  private UUID id;
//
//  private UUID application;
//
//  private UUID account;
//
//  private byte[] secret;
//
//  public String getSecretAsString() {
//    return secret == null ? null : BitTwiddler.tob64str(secret);
//  }
//
//}
