package com.netradius.iam.directauth.data;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
@ToString(exclude = "secret") // For security
public class AuthData {

  @Id
  private UUID id;

  private UUID application;

  private UUID account;

  private byte[] secret;

  private Boolean allowed;

  public String getSecretAsString() {
    return secret == null ? null : BitTwiddler.tob64str(secret);
  }

}
