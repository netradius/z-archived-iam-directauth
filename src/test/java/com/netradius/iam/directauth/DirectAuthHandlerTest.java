package com.netradius.iam.directauth;

import com.netradius.authlib.direct.DirectAuthSecret;
import com.netradius.authlib.direct.DirectAuthType;
import com.netradius.iam.directauth.handler.DirectAuthHandler;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class DirectAuthHandlerTest {

  private static final String SERVER_KEY = "S0pdHNsMnxCzlQJkVTUODFqSC+JPvJuMImAPz4E1rZPZt5aLDe3rZIEs2WZk3CY+111WBX+Ej990BpMi1asuvg==";
  private static final UUID SERVER_KEY_ID = UUID.fromString("5cb5acc7-6fb7-409c-be17-319fb7cb9568");
  private static final String CLIENT_KEY = "frUkGTiG6xm/7dGb5wmStuMIbZiYgPvVdYZTu2E8SkCAdq91ou2RMItA+L/iL2XilVttd7yzC9yQZSWzM4Ancg==";
  private static final UUID CLIENT_KEY_ID = UUID.fromString("c15c50fb-e089-4cb9-a418-5c0221dfced7");

  @LocalServerPort
  private int port;

  private WebClient client;
  private DirectAuthSecret serverSecret;
  private DirectAuthSecret clientSecret;

  @PostConstruct
  public void init() {
    this.client = WebClient.builder().baseUrl("http://localhost:" + port).build();
    this.serverSecret = DirectAuthSecret.builder().secretKey(SERVER_KEY).keyId(SERVER_KEY_ID).build();
    this.clientSecret = DirectAuthSecret.builder().secretKey(CLIENT_KEY).keyId(CLIENT_KEY_ID).build();
  }

  private String getHeader(ClientResponse response, String name) {
    List<String> list = response.headers().header(name);
    return list.isEmpty() ? null : list.get(0);
  }

  private void logBodyIfNotOk(ClientResponse response) {
    if (response.statusCode() != HttpStatus.OK) {
      log.info(String.format("Received status code %d", response.rawStatusCode()));
      String body = response.body(BodyExtractors.toMono(String.class)).block();
      log.info(String.format("Received body: %s", body));
    }
  }

  @Test
  public void happyPath() {
    String authorization = serverSecret.authorization("nr/iam", "/authorize/direct").getHeaderValue();
    String audience = "nr/hello";
    String path = "/hello";
    String clientAuthorization = clientSecret.authorization(audience, path).getHeaderValue();
    String action = "Hello";

    ClientResponse response = client.post()
        .uri(DirectAuthHandler.PATH)
        .header("authorization", authorization)
        .header("authorization-client", clientAuthorization)
        .header("authorization-audience", audience)
        .header("authorization-path", path)
        .header("authorization-action", action)
        .exchange().block();

    assertThat(response, notNullValue());
    logBodyIfNotOk(response);
    assertThat(getHeader(response, "account"), notNullValue());
    assertThat(getHeader(response, "application"), notNullValue());
    assertThat(response.statusCode(), equalTo(HttpStatus.OK));
  }

  @Test
  public void testBadServerKey() {
    String audience = "nr/hello";
    String path = "/hello";
    DirectAuthSecret secret = DirectAuthSecret.generate(DirectAuthType.V1_HMAC_SHA512);
    String authorization = secret.authorization("nr/iam", "/authorize/direct").getHeaderValue();
    String clientAuthorization = clientSecret.authorization(audience, path).getHeaderValue();
    String action = "Hello";

    ClientResponse response = client.post()
        .uri(DirectAuthHandler.PATH)
        .header("authorization", authorization)
        .header("authorization-client", clientAuthorization)
        .header("authorization-audience", audience)
        .header("authorization-path", "/hello")
        .header("authorization-action", action)
        .exchange().block();

    assertThat(response, notNullValue());
    logBodyIfNotOk(response);
    assertThat(response.statusCode(), equalTo(HttpStatus.UNAUTHORIZED));
  }
}
