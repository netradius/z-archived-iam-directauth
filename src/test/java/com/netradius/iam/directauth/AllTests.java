package com.netradius.iam.directauth;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    HealthHandlerTest.class,
    DirectAuthHandlerTest.class
})
public class AllTests {
}
