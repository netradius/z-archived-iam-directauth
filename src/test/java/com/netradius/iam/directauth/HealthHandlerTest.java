package com.netradius.iam.directauth;

import com.netradius.iam.directauth.handler.HealthHandler;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class HealthHandlerTest {

  @LocalServerPort
  private int port;

  private WebClient client;

  @PostConstruct
  public void init() {
    this.client = WebClient.builder().baseUrl("http://localhost:" + port).build();
  }

  @Test
  public void testShallow() {
    ClientResponse response = client.get().uri(HealthHandler.SHALLOW_PATH).exchange().block();
    assertThat(response, notNullValue());
    assertThat(response.statusCode(), equalTo(HttpStatus.OK));
  }

  @Test
  public void testDeep() {
    ClientResponse response = client.get().uri(HealthHandler.DEEP_PATH).exchange().block();
    assertThat(response, notNullValue());
    assertThat(response.statusCode(), equalTo(HttpStatus.OK));
  }
}
