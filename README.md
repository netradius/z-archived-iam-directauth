# NetRadius IAM DirectAuth Service

This project provides the implementation for the /authorize/direct API call used
for server to server direct authentication and authorization.
